export default {
  STORE_MERMAID_SCRIPT(state, mermaidScript) {
    Object.assign(state, { mermaidScript })
  },

  STORE_YAML_AST(state, yamlAST) {
    Object.assign(state, { yamlAST })
  },

  STORE_YAML_CONTENT(state, yamlContent) {
    Object.assign(state, { yamlContent })
  }
}
